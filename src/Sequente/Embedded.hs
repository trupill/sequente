{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# language PartialTypeSignatures #-}
{-# language GADTs, DataKinds, TypeFamilies, KindSignatures, TypeOperators, PolyKinds #-}
module Sequente.Embedded where

import Data.Proxy
import Data.Functor.Const
import Data.Type.Equality
import Data.Foldable

data KindOfTy = K_Data | K_CoData deriving (Eq, Show)

class KindOf ty where
  kindOf   :: Proxy ty -> KindOfTy
  isData   :: Proxy ty -> Bool
  isData   = (== K_Data) . kindOf
  isCoData :: Proxy ty -> Bool
  isCoData = (== K_CoData) . kindOf

class DoTheMatch datas where
  match :: datas args  coargs  ty
        -> datas args' coargs' ty
        -> Maybe (args :~: args' , coargs :~: coargs')

data Peano

instance KindOf [a] where
  kindOf _ = K_Data
instance KindOf () where
  kindOf _ = K_Data
instance KindOf Peano where
  kindOf _ = K_Data
instance KindOf (a -> b) where
  kindOf _ = K_CoData

data DatasDef :: [*] -> [*] -> * -> * where
  ListNil  :: DatasDef '[]       '[] [a]
  ListCons :: DatasDef '[a, [a]] '[] [a]
  Unit     :: DatasDef '[]       '[] ()
  NatZ     :: DatasDef '[]       '[] Peano
  NatS     :: DatasDef '[Peano]  '[] Peano
deriving instance Show (DatasDef i j k)

instance DoTheMatch DatasDef where
  match ListNil ListNil = Just (Refl, Refl)
  match ListCons ListCons = Just (Refl , Refl)
  match Unit Unit = Just (Refl , Refl)
  match NatS NatS = Just (Refl , Refl)
  match NatZ NatZ = Just (Refl , Refl)
  match _ _ = Nothing

infixr 5 :.:
data CoDatasDef :: [*] -> [*] -> * -> * where
  (:.:)  :: CoDatasDef '[a] '[b] (a -> b)
  Toppie :: CoDatasDef '[] '[] a
deriving instance Show (CoDatasDef i j k)

instance DoTheMatch CoDatasDef where
  match (:.:) (:.:) = Just (Refl , Refl)
  match Toppie Toppie = Just (Refl , Refl)
  match _ _ = Nothing

type family (xs :: [k]) :++: (ys :: [k]) :: [k] where
  '[]       :++: ys = ys
  (x ': xs) :++: ys = x ': (xs :++: ys)

data Ix (xs :: [k]) (ty :: k) where
  Ix_Z :: Ix (x ': xs) x
  Ix_S :: Ix xs y -> Ix (x ': xs) y 
deriving instance Show (Ix xs ty)

infixr 4 :*:
data NP (f :: k -> *) (xs :: [k]) :: * where
  NP0   :: NP f '[]
  (:*:) :: f x -> NP f xs -> NP f (x ': xs)
deriving instance (forall x . Show (f x)) => Show (NP f xs)

npMap2 :: (forall x . f i j x -> g i' j' x) -> NP (f i j) xs -> NP (g i' j') xs
npMap2 f NP0 = NP0
npMap2 f (x :*: xs) = f x :*: npMap2 f xs

lkup :: NP f xs -> Ix (xs :++: ys) k -> Either (Ix ys k) (f k)
lkup NP0        i = Left i
lkup (x :*: xs) Ix_Z     = Right x
lkup (x :*: xs) (Ix_S i) = lkup xs i

data Term datas codatas gamma delta ty where
  Tm_Var  :: Ix gamma ty
          -> Term datas codatas gamma delta ty
  Tm_Mu   :: KindOf ty => [Alt CoPattern datas codatas gamma delta ty]
          -> Term datas codatas gamma delta ty
  Tm_Data :: datas args coargs ty
          -> NP (Term   datas codatas gamma delta) args
          -> NP (CoTerm datas codatas gamma delta) coargs
          -> Term datas codatas gamma delta ty
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y))
  => Show (Term d c g l t)

infixr 4 :**:
data NPAccum (f :: [i] -> [j] -> k -> *) :: [i] -> [j] -> [k] -> * where
  NPAccum0 :: NPAccum f '[] '[] '[]
  (:**:)   :: f i0 j0 x -> NPAccum f is js xs
           -> NPAccum f (i0 :++: is) (j0 :++: js) (x ': xs)
deriving instance (forall a b c . Show (f a b c)) => Show (NPAccum f i j x)

data Pattern datas codatas gamma delta ty where
  P_Name :: String -> Pattern datas codatas gamma delta ty
                   -> Pattern datas codatas gamma delta ty
  P_Var  :: Pattern datas codatas '[ty] '[] ty
  P_Data :: datas args coargs ty
         -> NPAccum (Pattern   datas codatas) gamma1 delta1 args
         -> NPAccum (CoPattern datas codatas) gamma2 delta2 coargs
         -> Pattern datas codatas (gamma1 :++: gamma2) (delta1 :++: delta2) ty
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y))
  => Show (Pattern d c g t y)

data CoTerm datas codatas gamma delta ty where
  CoTm_Var    :: Ix delta ty
              -> CoTerm datas codatas gamma delta ty
  CoTm_Mu     :: KindOf ty => [Alt Pattern datas codatas gamma delta ty]
              -> CoTerm datas codatas gamma delta ty
  CoTm_CoData :: codatas args coargs ty
              -> NP (Term   datas codatas gamma delta) args
              -> NP (CoTerm datas codatas gamma delta) coargs
              -> CoTerm datas codatas gamma delta ty
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y))
  => Show (CoTerm c d g t y)

data CoPattern datas codatas gamma delta ty where
  CoP_Name   :: String -> CoPattern datas codatas gamma delta ty
                       -> CoPattern datas codatas gamma delta ty
  CoP_Var    :: CoPattern datas codatas '[] '[ty] ty
  CoP_CoData :: codatas args coargs ty
             -> NPAccum (Pattern   datas codatas) gamma1 delta1 args
             -> NPAccum (CoPattern datas codatas) gamma2 delta2 coargs
             -> CoPattern datas codatas (gamma1 :++: gamma2) (delta1 :++: delta2) ty
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y))
  => Show (CoPattern c d g t y)

data Alt pat datas codatas gamma delta ty where
  (:=>:) :: (IsList gamma' , IsList delta')
         => pat datas codatas gamma' delta' ty
         -> Command datas codatas (gamma' :++: gamma) (delta' :++: delta)
         -> Alt pat datas codatas gamma delta ty
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y)
                  ,forall gamma delta . Show (pat d c gamma delta y))
  => Show (Alt pat d c g t y)

getg' :: f datas codatas gamma' delta' ty -> Proxy gamma'
getg' _ = Proxy

getd' :: f datas codatas gamma' delta' ty -> Proxy delta'
getd' _ = Proxy

data Command datas codatas gamma delta where
  (:||:) :: Term    datas codatas gamma delta ty
         -> CoTerm  datas codatas gamma delta ty
         -> Command datas codatas gamma delta
deriving instance (forall a ca y . Show (d a ca y) , forall a ca y . Show (c a ca y))
  => Show (Command d c g t)

-----------------------------------
-- * evaluator
----------

------------------------------------
-- List singletons

class IsList (l :: [k]) where
  sing :: Proxy l -> SList l
instance IsList '[] where
  sing _ = SL_Nil
instance IsList xs => IsList (x ': xs) where
  sing _ = SL_Cons (sing Proxy)

data SList :: [k] -> * where
  SL_Nil  :: SList '[]
  SL_Cons :: SList xs -> SList (x ': xs)

lkupBuffer :: SList buffer -> SList ys
           -> NP terms xs -> Ix (buffer :++: (xs :++: ys)) k
           -> Either (Ix (buffer :++: ys) k) (terms k)
lkupBuffer SL_Nil ys      ts ix = lkup ts ix
lkupBuffer (SL_Cons b) ys ts Ix_Z = Left Ix_Z
lkupBuffer (SL_Cons b) ys ts (Ix_S ix) =
  case lkupBuffer b ys ts ix of
    Right r  -> Right r
    Left ix' -> Left (Ix_S ix')

fcong :: (SList gamma :~: SList gamma')
      -> (SList delta :~: SList delta')
      -> f datas codatas gamma  delta  ty
      -> f datas codatas gamma' delta' ty
fcong Refl Refl = id
      
assoc :: SList a -> SList b -> Proxy c
      -> SList (a :++: (b :++: c)) :~: SList ((a :++: b) :++: c)
assoc SL_Nil       _   _ = Refl
assoc (SL_Cons sa) sb sc = case assoc sa sb sc of
                             Refl -> Refl

--------------------------------------------------------
-- * Injections

class InjOn f where
  inj :: (forall k . Ix gamma k -> Ix gamma' k)
      -> (forall k . Ix delta k -> Ix delta' k)
      -> f datas codatas gamma  delta  ty
      -> f datas codatas gamma' delta' ty

ixAdd :: SList a -> Ix b k -> Ix (a :++: b) k
ixAdd SL_Nil       ix = ix
ixAdd (SL_Cons sa) ix = Ix_S (ixAdd sa ix)

ixAddAfter :: SList b
           -> (forall k . Ix a k -> Ix a' k)
           -> Ix (b :++: a)  k
           -> Ix (b :++: a') k
ixAddAfter SL_Nil       f x = f x
ixAddAfter (SL_Cons sb) f Ix_Z = Ix_Z
ixAddAfter (SL_Cons sb) f (Ix_S ix) = Ix_S (ixAddAfter sb f ix)

instance InjOn Term where
  inj gs ds (Tm_Var ix)  = Tm_Var $ gs ix
  inj gs ds (Tm_Mu alts) = Tm_Mu (map (inj gs ds) alts)
  inj gs ds (Tm_Data d a ca) = Tm_Data d (npMap2 (inj gs ds) a) (npMap2 (inj gs ds) ca)

instance InjOn CoTerm where
  inj gs ds (CoTm_Var ix)  = CoTm_Var $ ds ix
  inj gs ds (CoTm_Mu alts) = CoTm_Mu (map (inj gs ds) alts)
  inj gs ds (CoTm_CoData d a ca) = CoTm_CoData d (npMap2 (inj gs ds) a) (npMap2 (inj gs ds) ca)

instance InjOn (Alt pat) where
  inj gs ds (pat :=>: (t :||: ct)) =
    let sgamma' = sing $ getg' pat
        sdelta' = sing $ getd' pat
     in pat :=>: (inj (ixAddAfter sgamma' gs) (ixAddAfter sdelta' ds) t
             :||: inj (ixAddAfter sgamma' gs) (ixAddAfter sdelta' ds) ct)

---------------------------------------------------
-- * Substitutions

type SubstFor datas codatas gamma delta gs ds
  = ( NP (Term   datas codatas gamma delta) gs
    , NP (CoTerm datas codatas gamma delta) ds)

substForProxys :: SubstFor datas codatas gamma delta gs ds
               -> (Proxy gs , Proxy ds , Proxy gamma , Proxy delta)
substForProxys _ = (Proxy , Proxy , Proxy , Proxy)

class SubstOn f where
  subst :: (SList gs'  , SList ds')
        -> (SList gamma, SList delta) 
        -> SubstFor datas codatas gamma delta gs ds
        -> f datas codatas (gs' :++: (gs :++: gamma)) (ds' :++: (ds :++: delta)) ty
        -> f datas codatas (gs' :++: gamma)           (ds' :++: delta)           ty

subst' :: (IsList gamma , IsList delta , SubstOn f)
       => SubstFor datas codatas gamma delta gs ds
       -> f datas codatas (gs :++: gamma) (ds :++: delta) ty
       -> f datas codatas gamma delta ty
subst' = subst (SL_Nil , SL_Nil) (sing $ Proxy , sing $ Proxy)

instance SubstOn Term where
  subst (gs' , ds') (gamma , delta) (ts, cs) (Tm_Var ix) =
    case lkupBuffer gs' gamma ts ix of
      Left ix' -> Tm_Var ix'
      Right t  -> inj (ixAdd gs') (ixAdd ds') t
  subst over gd sf (Tm_Data d a ca)
    = Tm_Data d (npMap2 (subst over gd sf) a)
                (npMap2 (subst over gd sf) ca)
  subst over gd sf (Tm_Mu alts) = Tm_Mu (map (subst over gd sf) alts)

instance SubstOn CoTerm where
  subst (gs' , ds') (gamma , delta) (ts, cs) (CoTm_Var ix) =
    case lkupBuffer ds' delta cs ix of
      Left ix' -> CoTm_Var ix'
      Right t  -> inj (ixAdd gs') (ixAdd ds') t
  subst over gd sf (CoTm_CoData d a ca)
    = CoTm_CoData d (npMap2 (subst over gd sf) a)
                (npMap2 (subst over gd sf) ca)
  subst over gd sf (CoTm_Mu alts) = CoTm_Mu (map (subst over gd sf) alts)

instance SubstOn (Alt pat) where
  subst (sg' , sd') gd sf (pat :=>: (t :||: ct)) =
    let pgamma'   = getg' pat
        pdelta'   = getd' pat
        (pg , pd , pgamma , pdelta) = substForProxys sf
        assoc4   = fcong (assoc' pgamma' sg' pg pgamma) (assoc' pdelta' sd' pd pdelta)
        unassoc3 = fcong (sym (assoc (sing pgamma') sg' pgamma))
                         (sym (assoc (sing pdelta') sd' pdelta))
        assoc4'   = fcong (assoc' pgamma' sg' pg pgamma) (assoc' pdelta' sd' pd pdelta)
        unassoc3' = fcong (sym (assoc (sing pgamma') sg' pgamma))
                          (sym (assoc (sing pdelta') sd' pdelta))

        sgammag' = sumSList (sing pgamma') sg'
        sdeltad' = sumSList (sing pdelta') sd'
     in pat :=>:  (unassoc3  (subst (sgammag' , sdeltad') gd sf (assoc4  t))
             :||: (unassoc3' (subst (sgammag' , sdeltad') gd sf (assoc4' ct))))
   where                        
    assoc' :: (IsList a)
           => Proxy a -> SList b -> Proxy c -> Proxy d
           -> SList (a :++: (b :++: (c :++: d)))
          :~: SList ((a :++: b) :++: (c :++: d))
    assoc' pa sb pc pd = assoc (sing pa) sb (sump pc pd)

    sump :: Proxy a -> Proxy b -> Proxy (a :++: b)
    sump _ _ = Proxy

    sumSList :: SList a -> SList b -> SList (a :++: b)
    sumSList SL_Nil sb = sb
    sumSList (SL_Cons sa) sb = SL_Cons (sumSList sa sb)

-- npZipWithAccum :: NP (f i j) xs -> NPAccum f i' j' xs -> 

concatNP :: NP f xs -> NP f ys -> NP f (xs :++: ys)
concatNP NP0        ys = ys
concatNP (x :*: xs) ys = x :*: (concatNP xs ys)

pmatch :: (DoTheMatch datas , DoTheMatch codatas)
       => Pattern datas codatas gamma' delta' ty
       -> Term datas codatas gamma delta ty
       -> Maybe (SubstFor datas codatas gamma delta gamma' delta')
pmatch (P_Name _ p) t = pmatch p t
pmatch P_Var t = Just (t :*: NP0 , NP0)
pmatch (P_Data d a ca) (Tm_Data d' a' ca') = do
  (Refl , Refl) <- match d d'
  (s0 , s1)  <- pmatchNP a  a'
  (cs0, cs1) <- cmatchNP ca ca'
  return (concatNP s0 cs0 , concatNP s1 cs1)
pmatch _ _ = Nothing

cmatch :: (DoTheMatch datas , DoTheMatch codatas)
       => CoPattern datas codatas gamma' delta' ty
       -> CoTerm datas codatas gamma delta ty
       -> Maybe (SubstFor datas codatas gamma delta gamma' delta')
cmatch (CoP_Name _ p) t = cmatch p t
cmatch CoP_Var t = Just (NP0 , t :*: NP0)
cmatch (CoP_CoData d a ca) (CoTm_CoData d' a' ca') = do
  (Refl , Refl) <- match d d'
  (s0 , s1)  <- pmatchNP a  a'
  (cs0, cs1) <- cmatchNP ca ca'
  return (concatNP s0 cs0 , concatNP s1 cs1)
cmatch _ _ = Nothing

pmatchNP :: (DoTheMatch datas , DoTheMatch codatas)
         => NPAccum (Pattern datas codatas) gamma' delta' tys
         -> NP (Term datas codatas gamma delta) tys
         -> Maybe (SubstFor datas codatas gamma delta gamma' delta')
pmatchNP NPAccum0 NP0 = return (NP0 , NP0)
pmatchNP (p :**: ps) (x :*: xs) = do
  (s0 , s1)  <- pmatch p x
  (cs0, cs1) <- pmatchNP ps xs
  return (concatNP s0 cs0 , concatNP s1 cs1)

cmatchNP :: (DoTheMatch datas , DoTheMatch codatas)
         => NPAccum (CoPattern datas codatas) gamma' delta' tys
         -> NP (CoTerm datas codatas gamma delta) tys
         -> Maybe (SubstFor datas codatas gamma delta gamma' delta')
cmatchNP NPAccum0 NP0 = return (NP0 , NP0)
cmatchNP (p :**: ps) (x :*: xs) = do
  (s0 , s1)  <- cmatch p x
  (cs0, cs1) <- cmatchNP ps xs
  return (concatNP s0 cs0 , concatNP s1 cs1)

substOnCmd :: (IsList gamma , IsList delta)
           => Command datas codatas (gs :++: gamma) (ds :++: delta)
           -> SubstFor datas codatas gamma delta gs ds
           -> Command datas codatas gamma delta
substOnCmd (t :||: ct) sigma = (subst' sigma t :||: subst' sigma ct)

eval :: (DoTheMatch datas , DoTheMatch codatas , forall i j t . Show (datas i j t) , forall i j t . Show (codatas i j t))
     => Command datas codatas '[] '[]
     -> Command datas codatas '[] '[]

eval (tm@(Tm_Mu alts :: Term _ _ _ _ ty) :||: cotm@(CoTm_Mu coalts))
  = case kindOf (Proxy :: Proxy ty) of
      K_Data   -> case alts of
                    [CoP_Var :=>: c] -> eval $ substOnCmd c (NP0, cotm :*: NP0)
                    _ -> error $ "Unexpected pattern:\n" ++ unlines (map (\(pat :=>: _) -> show pat) alts)
      K_CoData -> case coalts of
                    [P_Var :=>: c] -> eval $ substOnCmd c (tm :*: NP0, NP0)
                    _ -> error $ "Unexpected pattern:\n" ++ unlines (map (\(pat :=>: _) -> show pat) alts)

eval (Tm_Mu alts :||: ct) =
  -- Do the substitution in the command
  case asum (map (\(pat :=>: c) -> substOnCmd c <$> cmatch pat ct) alts) of
    Nothing -> error $ "No match in mu:\n" ++ unlines (map (\(pat :=>: _) -> show pat) alts)
    Just r  -> eval r

eval (t :||: CoTm_Mu alts) =
  -- Do the substitution in the command
  case asum (map (\(pat :=>: c) -> substOnCmd c <$> pmatch pat t) alts) of
    Nothing -> error $ "No match in comu:\n" ++ unlines (map (\(pat :=>: _) -> show pat) alts)
    Just r  -> eval r

-- base case
eval t = t

------------------------------------
-- Finally, the evaluator!


