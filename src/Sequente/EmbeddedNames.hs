{-# language GADTs, DataKinds, TypeFamilies,
    KindSignatures, TypeOperators, PolyKinds,
    RankNTypes, TypeApplications, AllowAmbiguousTypes,
    UndecidableInstances #-}
module Sequente.EmbeddedNames where

import GHC.TypeLits

data DatasDef :: [*] -> [*] -> * -> * where
  ListNil  :: DatasDef '[]       '[] [a]
  ListCons :: DatasDef '[a, [a]] '[] [a]

data CoDatasDef :: [*] -> [*] -> * -> * where
  (:.:) :: CoDatasDef '[a] '[b] (a -> b)

type family (xs :: [k]) :++: (ys :: [k]) :: [k] where
  '[]       :++: ys = ys
  (x ': xs) :++: ys = x ': (xs :++: ys)

{-
type family IsThere (xs :: [(Symbol, k)]) (nm :: Symbol) :: Constraint where
  IsThere ( '(nm,ty) ': rest ) nm = ()
  IsThere ( x        ': rest ) nm = IsThere rest nm
-}

type family Find (xs :: [(Symbol, k)]) (nm :: Symbol) :: k where
  Find '[]                  nm = TypeError (Text "Name " :<>: ShowType nm :<>: Text " is not in scope")
  Find ( '(nm,ty) ': rest ) nm = ty
  Find ( x        ': rest ) nm = Find rest nm

data NP (f :: k -> *) (xs :: [k]) :: * where
  NP0   :: NP f '[]
  (:*:) :: f x -> NP f xs -> NP f (x ': xs)

data Term datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) (ty :: k) where
  Tm_Var  :: forall nm datas codatas gamma delta.
             Term datas codatas gamma delta (Find gamma nm)
  Tm_Mu   :: [Alt CoPattern datas codatas gamma delta ty]
          -> Term datas codatas gamma delta ty
  Tm_Data :: datas args coargs ty
          -> NP (Term   datas codatas gamma delta) args
          -> NP (CoTerm datas codatas gamma delta) coargs
          -> Term datas codatas gamma delta ty

data NPAccum (f :: [i] -> [j] -> k -> *) (xs :: [k]) :: [i] -> [j] -> * where
  NPA0   :: NPAccum f '[] '[] '[]
  (:**:) :: f i0 j0 x -> NPAccum f xs is js
         -> NPAccum f (x ': xs) (i0 :++: is) (j0 :++: js) 

data Pattern datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) (ty :: k) where
  P_Var  :: forall nm ty datas codatas. Pattern datas codatas '[ '(nm,ty) ] '[] ty
  P_Data :: datas args coargs ty
         -> NPAccum (Pattern   datas codatas) args   gamma1 delta1
         -> NPAccum (CoPattern datas codatas) coargs gamma2 delta2
         -> Pattern datas codatas (gamma1 :++: gamma2) (delta1 :++: delta2) ty

data CoTerm datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) (ty :: k) where
  CoTm_Var    :: forall nm datas codatas gamma delta.
                 CoTerm datas codatas gamma delta (Find delta nm)
  CoTm_Mu     :: [Alt Pattern datas codatas gamma delta ty]
              -> CoTerm datas codatas gamma delta ty
  CoTm_CoData :: codatas args coargs ty
              -> NP (Term   datas codatas gamma delta) args
              -> NP (CoTerm datas codatas gamma delta) coargs
              -> CoTerm datas codatas gamma delta ty

data CoPattern datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) (ty :: k) where
  CoP_Var    :: forall nm ty datas codatas. CoPattern datas codatas '[] '[ '(nm,ty) ] ty
  CoP_CoData :: codatas args coargs ty
             -> NPAccum (Pattern   datas codatas) args   gamma1 delta1
             -> NPAccum (CoPattern datas codatas) coargs gamma2 delta2
             -> CoPattern datas codatas (gamma1 :++: gamma2) (delta1 :++: delta2) ty

data Alt pat datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) (ty :: k) where
  (:=>:) :: pat datas codatas gamma' delta' ty
         -> Command datas codatas (gamma' :++: gamma) (delta' :++: delta)
         -> Alt pat datas codatas gamma delta ty

data Command datas codatas (gamma :: [(Symbol,k)]) (delta :: [(Symbol,k)]) where
  (:||:) :: Term    datas codatas gamma delta ty
         -> CoTerm  datas codatas gamma delta ty
         -> Command datas codatas gamma delta

type DefCommand = Command DatasDef CoDatasDef '[] '[]

f *.* g = CoP_CoData (:.:) (f :**: NPA0) (g :**: NPA0)

ident :: Term DatasDef CoDatasDef '[] '[] (a -> a)
ident = Tm_Mu [ (P_Var @"x" *.* CoP_Var @"alpha")
                :=>: (Tm_Var @"x" :||: CoTm_Var @"alpha")
              ]
