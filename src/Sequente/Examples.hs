{-# language GADTs, DataKinds #-}
{-# language PartialTypeSignatures #-}
module Sequente.Examples where

import Sequente.Embedded

f *.* g = CoP_CoData (:.:) (f :**: NPAccum0) (g :**: NPAccum0)

infixr 3 *..*

f *..* g = CoTm_CoData (:.:) (f :*: NP0) (g :*: NP0)

n0 :: NPAccum f '[] '[] '[]
n0 = NPAccum0

foldTm :: KindOf b => Term DatasDef CoDatasDef gamma delta ([a] -> (a -> b -> b) -> b -> b)
--                                            f          x         alpha
foldTm = Tm_Mu [ (P_Data ListNil n0 n0) *.* (P_Var *.* (P_Var *.* CoP_Var))
                 :=>: (Tm_Var (Ix_S Ix_Z) :||: CoTm_Var Ix_Z)
--                                  x           xs                      f          z          alpha
               , (P_Data ListCons (P_Var :**: (P_Var :**: n0)) n0 *.* (P_Var *.* (P_Var *.* CoP_Var)))
                 :=>: (Tm_Var (Ix_S $ Ix_S Ix_Z)
                    :||: (Tm_Var Ix_Z *..* CoTm_Mu [ P_Var :=>:
                           (foldTm :||: (Tm_Var (Ix_S $ Ix_S Ix_Z)
                                         *..* Tm_Var (Ix_S $ Ix_S $ Ix_S Ix_Z)
                                         *..* Tm_Var (Ix_S $ Ix_S $ Ix_S $ Ix_S Ix_Z)
                                         *..* CoTm_Mu [ P_Var :=>:
                                                      ((Tm_Var $ Ix_S Ix_Z) :||: (Tm_Var Ix_Z *..* CoTm_Var Ix_Z))
                                                      ])
                                        )
                         ]))
               ]


succ' :: Term DatasDef CoDatasDef gamma delta (a -> Peano -> Peano)
succ' = Tm_Mu [ CoP_Name "succ1" (P_Var *.* CoP_Var)
                  :=>: (Tm_Mu [ CoP_Name "succ2" (P_Var *.* CoP_Var) :=>: (
                                   Tm_Data NatS (Tm_Var Ix_Z :*: NP0) NP0 :||: CoTm_Var Ix_Z) ]
                    :||: CoTm_Var Ix_Z) ]



{-
                                  )]
-}

zero = Tm_Data NatZ NP0 NP0

top = CoTm_CoData Toppie NP0 NP0

-- ident :: Term DatasDef CoDatasDef '[] '[] (a -> a)
ident = Tm_Mu [ (CoP_CoData (:.:) (P_Var :**: NPAccum0) (CoP_Var :**: NPAccum0))
                :=>:
                (Tm_Var Ix_Z :||: CoTm_Var Ix_Z)
              ]


nil = Tm_Data ListNil NP0 NP0

cons x xs = Tm_Data ListCons (x :*: (xs :*: NP0)) NP0

unit = Tm_Data Unit NP0 NP0

l = cons unit (cons unit (cons unit nil))

t :: CoPattern DatasDef CoDatasDef _ _ _
t = (P_Data ListCons (P_Var :**: (P_Var :**: n0)) n0 *.* (P_Var *.* (P_Var *.* CoP_Var)))

cp = (l *..* succ' *..* zero *..* top)

-- YAY! :D :D
res = eval (foldTm :||: (l *..* succ' *..* zero *..* top))
