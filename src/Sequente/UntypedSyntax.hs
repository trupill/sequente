{-# language DeriveGeneric #-}
{-# language MultiParamTypeClasses #-}
{-# language FlexibleContexts #-}
{-# language ConstraintKinds #-}
module Sequente.UntypedSyntax where

import Data.Typeable
import GHC.Generics
import Unbound.Generics.LocallyNameless

-- Language of terms and coterms with types taken from `t`

type   TermVar t = Name (  Term t)
type CoTermVar t = Name (CoTerm t)

data Term    t = Var      (TermVar t)
               | Abs      (Bind (CoTermVar t, Embed t) (Command t))
               -- Pattern match principle for term "a -> b"
               | AbsStack (Bind (TermVar t , CoTermVar t , Embed t) (Command t))
               deriving (Show, Generic)
data CoTerm  t = CoVar (CoTermVar t)
               | CoAbs (Bind (TermVar t, Embed t) (Command t))
               -- Constructors for coterm of type "a -> b"
               | (Term t) :.: (CoTerm t)
               | TopLevel
               deriving (Show, Generic)
data Command t = Term t :||: CoTerm t
               deriving (Show, Generic)

infixr 5 :.:
infix  4 :||:

type Fine t = (Typeable t, Alpha t, Subst (Term t) t, Subst (CoTerm t) t)

instance (Typeable t, Alpha t) => Alpha (Term t)
instance (Typeable t, Alpha t) => Alpha (CoTerm t)
instance (Typeable t, Alpha t) => Alpha (Command t)

instance (Fine t) => Subst (  Term t) (  Term t) where
  isvar (Var x) = Just $ SubstName x
  isvar _       = Nothing
instance (Fine t) => Subst (CoTerm t) (CoTerm t) where
  isvar (CoVar x) = Just $ SubstName x
  isvar _         = Nothing
instance (Fine t) => Subst (CoTerm t) (  Term t)
instance (Fine t) => Subst (  Term t) (CoTerm t)
instance (Fine t) => Subst (  Term t) (Command t)
instance (Fine t) => Subst (CoTerm t) (Command t)

data ReductionOrder = CallByValue | CallByName

eval :: (Fresh m, Fine t) => ReductionOrder -> Command t -> m (Command t)
eval r (Abs v :||: CoAbs e)
  | CallByValue <- r = do
    -- Priority to Abs redexes
    ((alpha, _), c) <- unbind v
    return $ subst alpha (CoAbs e) c
  | CallByName  <- r = do
    -- Priority to CoAbs redexes
    ((x, _), c) <- unbind e
    return $ subst x (Abs v) c
eval _ (Abs v :||: e) = do
  ((alpha, _), c) <- unbind v
  return $ subst alpha e c
eval _ (v :||: CoAbs e) = do
  ((x, _), c) <- unbind e
  return $ subst x v c
-- Evaluation Rule for ->
eval _ (AbsStack v :||: t :.: e) = do
  ((x , alpha , _), c) <- unbind v
  return $ subst x t $ subst alpha e c

-------------------------------------
-- Lambda calculus, as we know it.

var :: String -> Term ()
var x = Var (s2n x)

app :: Term () -> Term () -> Term ()
app t u = let alpha = s2n "alpha"
           in Abs $ bind (alpha , embed ()) (t :||: u :.: CoVar alpha)

lambda :: String -> Term () -> Term ()
lambda x t = let xv    = s2n x
                 alpha = s2n "alpha"
              in AbsStack $ bind (xv , alpha , embed ()) (t :||: CoVar alpha)

cmd :: Term () -> Command ()
cmd t = (t :||: TopLevel)

evalF :: Command () -> Command ()
evalF = runFreshM . eval CallByName 

--------------------
-- Example

t1 :: Term ()
t1 = app (lambda "x" (app (var "x") (var "x"))) (var "z")

t1' = evalF (cmd t1)

t1'' = evalF t1'


